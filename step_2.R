library(tidyr)               
library(forcats)         
library(janitor)                  
library(dplyr)             
     
                  
shapefile = readRDS(paste0(USR_DIR, "brodri/shapefile_fr_de.rds"))                     
                    
data_final = readRDS(paste0(USR_DIR, "brodri/data_final_status.rds"))             
           
unemp = readRDS(paste0(USR_DIR, "brodri/unemp.rds"))             
     
     
     
# Add the age category to the data     
data_final <- data_final %>%     
    mutate(age_class = case_when(     
        .$age %in% seq(0, 14) ~ "<14",     
        .$age %in% seq(15, 19) ~ "15 - 19",     
        .$age %in% seq(20, 24) ~ "20 - 24",     
        .$age %in% seq(25, 30) ~ "25 - 30",     
        .$age %in% seq(31, 34) ~ "31 - 34",     
        .$age %in% seq(35, 39) ~ "35 - 39",     
        .$age %in% seq(40, 44) ~ "40 - 44",     
        .$age %in% seq(45, 49) ~ "45 - 49",     
        .$age %in% seq(50, 54) ~ "50 - 54",     
        .$age %in% seq(55, 59) ~ "55 - 59",     
        .$age %in% seq(60, 64) ~ "60 - 64",     
        .$age %in% seq(65, 120) ~ "65+")) %>%     
    filter(enroll != "[100]currently enrolled") %>%     
    filter(cmas != "[220]in education")     
     
     
# Create a birth variable     
data_final$birth <- if_else(data_final$ageyoch == 0, 1, 0)     
     
     
fertile_years = function(age){     
  ifelse(age < 50, age - 15, 34)     
}     
     
glimpse(data_final)    
    
reg_data <- data_final %>%     
    mutate(cmas = dplyr::recode(cmas, "[100]mainly employed" = "Mainly employed")) %>%     
    mutate(cmas = dplyr::recode(cmas, "[200]not mainly employed" = "Unemployment")) %>%     
    mutate(cmas = dplyr::recode(cmas, "[210]retired, pensioner or rentier" = "Retired")) %>%     
    mutate(cmas = dplyr::recode(cmas, "[211]retired from job or business" = "Retired")) %>%    
    mutate(cmas = dplyr::recode(cmas, "[230]homemaker" = "Homemaker/Care for children")) %>%    
    mutate(cmas = dplyr::recode(cmas, "[231]care of children" = "Homemaker/Care for children")) %>%     
    mutate(cmas = dplyr::recode(cmas, "[250]in military or civil service" = "Other")) %>%     
    mutate(cmas = dplyr::recode(cmas, "[260]unemployed" = "Unemployment")) %>%    
    filter(cmas %in% c("Mainly employed", "Unemployment", "Homemaker/Care for children")) %>%    
    mutate(marital = dplyr::recode(marital, "[110]married" = "Married")) %>%     
    mutate(marital = dplyr::recode(marital, "[210]never married/not in union" = "Single person")) %>%     
    mutate(marital = dplyr::recode(marital, "[221]separated" = "Separated/divorced")) %>%     
    mutate(marital = dplyr::recode(marital, "[222]divorced" = "Separated/divorced")) %>%     
    mutate(status1 = dplyr::recode(status1, "[100]dependent employed" = "Employee")) %>%     
    mutate(status1 = dplyr::recode(status1, "[110]regular employee" = "Employee")) %>%     
    mutate(status1 = dplyr::recode(status1, "[120]non regular employee" = "Temporary worker/apprentice")) %>%     
    mutate(status1 = dplyr::recode(status1, "[121]temporary or occasional worker" = "Temporary worker/apprentice")) %>%    
    mutate(status1 = dplyr::recode(status1, "[122]apprentice / trainee" = "Temporary worker/apprentice")) %>%    
    mutate(status1 = dplyr::recode(status1, "[200]self-employed" = "Self-employed")) %>%    
    mutate(status1 = dplyr::recode(status1, "[210]employer" = "Self-employed")) %>%    
    mutate(status1 = dplyr::recode(status1, "[220]own-account worker" = "Self-employed")) %>%    
    mutate(status1 = dplyr::recode(status1, "[240]contributing family worker" = "Self-employed")) %>%    
    filter(marital %in% c("Married", "Single person", "Separated/divorced")) %>%     
    mutate(educ = dplyr::recode(educ, "[1]low" = "Low level")) %>%     
    mutate(educ = dplyr::recode(educ, "[2]medium" = "Medium level")) %>%     
    mutate(educ = dplyr::recode(educ, "[3]high" = "High level")) %>%     
    filter(educ %in% c("Low level", "Medium level", "High level")) %>%     
    mutate(immigr = dplyr::recode(immigr, "[0]not immigrant" = "Not immigrant")) %>%     
    mutate(immigr = dplyr::recode(immigr, "[1]immigrant" = "Immigrant")) %>%     
    mutate(everwork = dplyr::recode(everwork, "[0]never worked " = "Has never worked", "[1]ever worked in job or business" = "Has worked")) %>%     
    mutate(fertile_years = fertile_years(age)) %>%     
    mutate(hil = hil/1000000) %>%     
    mutate(pil = pil/1000000) %>%     
    mutate(cmas = as.factor(cmas)) %>%     
    #group_by(iso2) %>%     
    mutate(nchild = as.factor(nchildren)) %>%     
    mutate(nchild = fct_lump(nchild, prop = 0.05)) %>%     
    mutate(nchild = dplyr::recode(nchild, "Other" = "4")) %>%     
    mutate(nchild = as.integer(as.character(nchild))) %>%     
    filter(age %in% seq(20, 50)) %>%     
    ungroup()     
     
     
unemp = unemp %>%     
  janitor::clean_names() %>%     
  mutate(time = as.numeric(time)) %>%     
  select(time, geo, sex, value)     
     
     
unemp_f = unemp %>%     
  filter(sex == "Females") %>%     
  select(NUTS_ID = geo, time, value) %>%     
  filter(time %in% seq(2008, 2010)) %>%     
  spread(time, value) %>%     
  rename_at(vars(-NUTS_ID), funs(paste0("unemp_in_", .)))     
     
     
reg_data = full_join(reg_data, unemp_f, by = "NUTS_ID") %>%     
    filter(!is.na(nchild))     
     
glimpse(reg_data)    
    
    
saveRDS(reg_data, paste0(USR_DIR, "brodri/reg_data_status.rds"))   